# [Mermaid.js](https://mermaidjs.github.io)使ってみる

クラス図とシーケンス図とアクティビティ図と任意のグラフと他色々と書けるjsだけで動く描画エンジンという事で試してみる。

gitbookに簡単に書けるのもいいので。

<!--START doctoc こうかな？ -->
<!--END doctoc -->

## セットアップ

gitbookで使うときは[mermaidのドキュメント](https://mermaidjs.github.io/)に紹介されているものだと[gitbook-plugin-mermade](https://github.com/JozoVilcek/gitbook-plugin-mermaid)使ってbook.jsonに

```json
{
    "plugins": ["mermaid-gb3"]
}
```
て書けば良いらしい。

けどmermaidのドキュメント自体は別のやり方してるような...

## クラス図書いてみる

次のように書けば良い？

````markdown
  ```mermaid
  classDiagram
    Entry --> Account
    Entry --> Transaction
    Account : balance()
    Transaction : start
    Transaction : end
    Entry : amount
  ```
````

```mermaid
classDiagram
  Entry-->Account
  Entry-->Transaction
  Account: +balance()
  Transaction: -start
  Transaction: -end
  Entry: -amount
```